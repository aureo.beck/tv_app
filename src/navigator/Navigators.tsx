import React from 'react';
import { createStackNavigator } from '@react-navigation/stack';
import { Routes } from './Routes';

import ContentList from '../screens/ContentList/ContentList';
import ContentDetails from '../screens/ContentDetails/ContentDetails';
import Colors from '../constants/Colors';

const screenOptions = {
  gestureEnabled: false,
  headerShown: true,
  headerStyle: { 
    backgroundColor: Colors.backgroundList,
  },
  headerTintColor: Colors.title,
  headerShadowVisible: false,
};

const MainStack = createStackNavigator();
function MainNavigator() {
  return (
    <MainStack.Navigator
      initialRouteName={Routes.main.contentList}
      screenOptions={screenOptions}
      >
      <MainStack.Screen name={Routes.main.contentList} component={ContentList} options={{ title: '' }} />
      <MainStack.Screen name={Routes.main.contentDetails} component={ContentDetails} options={{ title: '' }} />
    </MainStack.Navigator>
  );
}

const MainModalStack = createStackNavigator();
function MainModalNavigator() {
  return (
    <MainModalStack.Navigator
      initialRouteName={Routes.main.rootStack}
      screenOptions={{
        gestureEnabled: false,
        headerShown: false,
        presentation: 'transparentModal',
      }}
    >
      <MainModalStack.Screen name={Routes.main.rootStack} component={MainNavigator} />
    </MainModalStack.Navigator>
  );
}

export default MainModalNavigator;
