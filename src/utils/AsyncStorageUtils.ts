
import AsyncStorage from '@react-native-async-storage/async-storage';

export function getItemAsyncStorage(key: string) {
  const getItemPromise = async (resolve, reject) => {
    try {
      const value = await AsyncStorage.getItem(key);
      if (value) {
        resolve(JSON.parse(value));
      } else {
        resolve([]);
      }
    } catch (error) {
      reject(error);
    }
  };

  return new Promise(getItemPromise);
}

export function setItemAsyncStorage(key: string, value: string) {
  const setItemPromise = async (resolve, reject) => {
    try {
      await AsyncStorage.setItem(key, JSON.stringify(value))
      resolve();
    } catch (error) {
      reject(error);
    }
  };

  return new Promise(setItemPromise);
}
