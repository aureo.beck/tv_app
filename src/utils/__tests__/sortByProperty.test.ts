import { sortByBooleanProperty } from '../sortByProperty';

const inputObjArray = [
  {
    propName1: false,
    propName2: true,
    propName3: false,
  },
  {
    propName1: true,
    propName2: false,
    propName3: true,
  },
  {
    propName1: false,
    propName2: false,
    propName3: true,
  },
];

describe('Testing sortByBooleanProperty', () => {
  it('Test numeric sortByBooleanProperty ascending propName1', async () => {
    const result = sortByBooleanProperty(inputObjArray, 'propName1', true);
    expect(result).toEqual([
      {
        propName1: false,
        propName2: true,
        propName3: false,
      },
      {
        propName1: false,
        propName2: false,
        propName3: true,
      },
      {
        propName1: true,
        propName2: false,
        propName3: true,
      },
    ]);
  });
});
