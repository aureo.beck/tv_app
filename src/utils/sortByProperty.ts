/* eslint-disable @typescript-eslint/no-explicit-any */

export const sortByBooleanProperty = (objs: Array<any>, prop: string, asc = false): Array<any> => {
  const newArray = objs.concat();
  const ascValue = asc ? 1 : -1;
  const decValue = asc ? -1 : 1;

  return newArray.sort((a, b) => {
    if (a[prop] ? 1 : 0 > b[prop] ? 1 : 0) {
      return ascValue;
    }
    if (b[prop] ? 1 : 0 > a[prop] ? 1 : 0) {
      return decValue;
    }
    return 0;
  });
};
