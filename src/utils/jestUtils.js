/* eslint-disable no-undef */
export function flushPromises() {
  return new Promise((resolve) => setImmediate(resolve));
}

export function sagaArgs(args) {
  return { args, context: null, fn: expect.anything() };
}
