
export default {
    searchPlaceholder: 'Search for movies or series',
    listTitle: 'Content List',
    connectionError: 'Ops! There is a connection issue!',
};
  