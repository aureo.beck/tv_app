  
export default {
  visibilityOff: 'visibility-off',
  favorite: 'favorite',
  favoriteBorder: 'favorite-border',
  star: 'star',
  search: 'search',
  cloudOff: 'cloud-off',
};
  