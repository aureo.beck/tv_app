
export const IMDB_SEARCH_URL = 'https://imdb-api.com/en/API/AdvancedSearch/k_nykzp4xj';
export const IMDB_FILTER_TYPES = ['feature','tv_movie','tv_series','tv_episode','tv_miniseries'];
