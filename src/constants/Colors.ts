export default {
  white: '#ffffff',
  iconColor: '#000000',
  backgroundList: '#020122',
  backgroundCard: '#020122',
  title: '#FFFDED',
  primarySelection: '#F18805',
  rating: '#F1AE05',
};