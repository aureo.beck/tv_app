
export default {
  montserratBold: 'Montserrat-Bold',
  montserratSemiBold: 'Montserrat-SemiBold',
  montserratMedium: 'Montserrat-Medium',
  montserratRegular: 'Montserrat-Regular',
  montserratExtraBold: 'Montserrat-ExtraBold',
  system: 'System',
};
