
import RNFetchBlob from 'rn-fetch-blob';
import { IMDB_SEARCH_URL, IMDB_FILTER_TYPES } from '../constants/ImdbApi';

export function fetchSearchResults(searchItem: string) {
  const searchContentsPromise = (resolve, reject) => {
    const titleFilter = searchItem ? `title=${searchItem}` : '';
    const typesFilter = `&title_type=${IMDB_FILTER_TYPES.join(',')}`
    const url = `${IMDB_SEARCH_URL}?${titleFilter}${typesFilter}`;

    RNFetchBlob.fetch('GET', url.replace(' ','%20'), {})
      .then((response) => {
        const { status } = response.info();
        if (status === 200) {
          resolve(response.json().results);
        } else {
          reject(response.json().errorMessage);
        }
      })
      .catch((error) => {
        reject(error);
      });
  };

  return new Promise(searchContentsPromise);
}
