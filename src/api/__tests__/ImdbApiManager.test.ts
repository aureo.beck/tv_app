import RNFetchBlob from 'rn-fetch-blob';

import { fetchSearchResults } from '../ImdbApiManager';
import { responseMock } from '../__mocks__/ImdbApiManager.mocks';
import { mockedContents } from '../../state/ContentList/__mocks__/ContentList.mocks';

jest.spyOn(RNFetchBlob, 'fetch')
  .mockImplementation(() => Promise.resolve(responseMock) as any);

beforeEach(() => {
  jest.clearAllMocks();
});

describe('Testing ImdbApiManager', () => {
  it('give fetchSearchResults is called, mockedContents should be returned', async () => {
    const result = await fetchSearchResults('');
    expect(result).toEqual(mockedContents);
  });

  it('give fetchSearchResults returns status different then 200, error message should be returned', async () => {
    jest.spyOn(RNFetchBlob, 'fetch')
      .mockImplementation(() => Promise.resolve({ ...responseMock, info: () => ({ status: 404 })}) as any);

    try {
      await fetchSearchResults('');
    } catch (error) {
      expect(error).toEqual('Error Message');
    }
  });

  it('give fetchSearchResults fails, error should be returned', async () => {
    jest.spyOn(RNFetchBlob, 'fetch')
      .mockImplementation(() => Promise.reject('Error!') as any);

    try {
      await fetchSearchResults('');
    } catch (error) {
      expect(error).toEqual('Error!');
    }
  });
});
