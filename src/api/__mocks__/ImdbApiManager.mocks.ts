
import { mockedContents } from '../../state/ContentList/__mocks__/ContentList.mocks';

export const responseMock = {
    "info": () => ({
        "status": 200,
    }),
    "json": () => ({
        "results": mockedContents,
        "errorMessage": "Error Message",
    }),
    "taskId": "b0t9vkmtroel8qbgl3q3m",
    "type": "utf8",
    "respInfo": {
        "status": 200,
        "taskId": "b0t9vkmtroel8qbgl3q3m",
        "redirects": [
            "https://imdb-api.com/en/API/AdvancedSearch/k_nykzp4xj?&title_type=feature,tv_movie,tv_series,tv_episode,tv_miniseries"
        ],
        "timeout": false,
        "headers": {
            "cf-ray": "74b93aad4d74d70e-CDG",
            "Server": "cloudflare",
            "Strict-Transport-Security": "max-age=2592000",
            "Content-Type": "application/json; charset=utf-8",
            "nel": "{\"success_fraction\":0,\"report_to\":\"cf-nel\",\"max_age\":604800}",
            "Alt-Svc": "h3=\":443\"; ma=86400, h3-29=\":443\"; ma=86400",
            "report-to": "{\"endpoints\":[{\"url\":\"https:\\/\\/a.nel.cloudflare.com\\/report\\/v3?s=I9%2BBLhmiJtf4%2FKx9ylqnyeKpVVPHlpRPQbejgTLf3zJC0Q7gtgDuFk6rNOYSb7juTT5lsAJdhKmys0SO3l6LbEdcbteUMN%2F2uPo8ZsJ3XmseSRK%2FbYvKVIM5oe1BWuY%3D\"}],\"group\":\"cf-nel\",\"max_age\":604800}",
            "Date": "Fri, 16 Sep 2022 11:23:23 GMT",
            "Content-Encoding": "br",
            "cf-cache-status": "DYNAMIC",
            "Vary": "Accept-Encoding"
        },
        "respType": "json",
        "state": "2",
        "rnfbEncode": "utf8"
    }
}

  