

import React, { useCallback } from 'react';
import {
  StyleSheet,
  View,
  Image,
  TouchableOpacity,
} from 'react-native';
import { useDispatch } from 'react-redux';
import { useNavigation } from '@react-navigation/native';

import Colors from '../../constants/Colors';
import ContentHeader from '../components/ContentHeader';
import ContentInfo from '../components/ContentInfo';
import { Routes } from '../../navigator/Routes';
import { setFavoriteId, removeFavoriteId, setHiddenId } from '../../state/ContentList/ContentList.actions';

import { ContentItemType } from '../../state/ContentList/ContentList.types';

interface Props {
  contentItem: ContentItemType;
}

const ContentItem = ({ contentItem }: Props) => {
  const {
    id,
    image,
    title,
    description,
    imDbRating,
    isFavorite
  } = contentItem;

  const dispatch = useDispatch();
  const navigation = useNavigation();

  const setIsHidden = useCallback(() => {
    dispatch(setHiddenId(id));
  }, []);

  const onFavoriteToggle = useCallback(() => {
    if (isFavorite) {
      dispatch(removeFavoriteId(id));
    } else {
      dispatch(setFavoriteId(id));
    }
  }, [isFavorite]);

  const onItemClick = useCallback(() => {
    navigation.navigate(Routes.main.contentDetails as never, { contentItem } as never);
  }, [contentItem]);

  return (
    <View style={styles.container}>
      <ContentHeader setIsHidden={setIsHidden} isFavorite={isFavorite} onFavoriteToggle={onFavoriteToggle} />
      <TouchableOpacity onPress={onItemClick}>
        <Image
          style={styles.imageStyle}
          source={{ uri: image }}
        />
        <ContentInfo title={title} description={description} imDbRating={imDbRating} />
      </TouchableOpacity>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    padding: 5,
    flexDirection: 'column',
    justifyContent: 'space-between',
  },
  contentInfo: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  ratingContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    padding: 10,
    width: '15%',
  },
  photoHeader: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    zIndex: 20,
  },
  imageStyle: {
    height: 160,
    flex: 1,
  },
  title: {
    fontSize: 20,
    color: Colors.title,
  },
  rating: {
    fontSize: 16,
    color: Colors.title,
    marginLeft: 2,
  },
  headerButton: {
    zIndex: 99,
    position: 'absolute',
    margin: 5,
  },
  leftHeaderButton: {
    left: 0,
  },
  rightHeaderButton: {
    right: 0,
  },
});

export default ContentItem;
