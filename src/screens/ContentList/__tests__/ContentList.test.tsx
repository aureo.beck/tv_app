import React from 'react';
import { render, screen, fireEvent, act } from '@testing-library/react-native'
import * as reactRedux from 'react-redux';
import NetInfo from '@react-native-community/netinfo';
import renderer from 'react-test-renderer';

import ContentList from '../ContentList';
import Accessibility from '../../../constants/Accessibility';
import { getContentsSearch, getFavoriteIds, getHiddenIds } from '../../../state/ContentList/ContentList.actions';
import { flushPromises } from '../../../utils/jestUtils';
import { mockedContents } from '../../../state/ContentList/__mocks__/ContentList.mocks';

jest.unmock('react-native');

const dispatchMock = jest.fn();
jest.spyOn(reactRedux, 'useDispatch')
  .mockImplementation(() => dispatchMock);

jest.spyOn(reactRedux, 'useSelector')
  .mockImplementation(() => mockedContents);

afterEach(() => {
  jest.clearAllMocks();
  NetInfo.fetch = () => Promise.resolve({ isConnected: true } as any);
});

describe('Testing ContentList component', () => {

  it('should correctly render', async () => {
    const tree = renderer.create(<ContentList />).toJSON();
    await flushPromises();

    expect(tree).toMatchSnapshot();
  });

  it('given value of isConnected is false, connection error components should be shown', async () => {
    NetInfo.fetch = () => Promise.resolve({ isConnected: false } as any);

    let tree;
    await act(() => {
      tree = renderer.create(<ContentList />);
    });
    
    await expect(tree.toJSON()).toMatchSnapshot();
  });

  it('given component has rendered, initial actions are dispatched', async () => {
    render(<ContentList />);
    await flushPromises();
    
    expect(dispatchMock).toHaveBeenCalledWith(getContentsSearch(''));
    expect(dispatchMock).toHaveBeenCalledWith(getFavoriteIds());
    expect(dispatchMock).toHaveBeenCalledWith(getHiddenIds());
  });

  it('given value of search input changes, getContentsSearch is dispatched', async () => {
    const inputSearch = 'Search value'

    render(<ContentList />)
    await flushPromises();

    fireEvent.changeText(screen.getByTestId(Accessibility.searchInput), inputSearch)
    expect(dispatchMock).toHaveBeenCalledWith(getContentsSearch(inputSearch));
  });

  it('given value of isConnected is false, textConnectionError should be shown', async () => {
    NetInfo.fetch = () => Promise.resolve({ isConnected: false } as any);
  
    render(<ContentList />)
    await flushPromises();

    expect(screen.getByLabelText(Accessibility.textConnectionError)).toBeTruthy();
  });

});
