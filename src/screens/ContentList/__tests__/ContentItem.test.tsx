import React from 'react';
import * as reactRedux from 'react-redux';
import * as reactNavigation from '@react-navigation/native';
import NetInfo from '@react-native-community/netinfo';
import renderer from 'react-test-renderer';
import { shallow } from 'enzyme';

import ContentItem from '../ContentItem';
import { flushPromises } from '../../../utils/jestUtils';
import { mockedContents } from '../../../state/ContentList/__mocks__/ContentList.mocks';
import { setFavoriteId, removeFavoriteId, setHiddenId } from '../../../state/ContentList/ContentList.actions';
import { Routes } from '../../../navigator/Routes';

jest.unmock('react-native');
jest.mock('@react-navigation/native');

const dispatchMock = jest.fn();
jest.spyOn(reactRedux, 'useDispatch')
  .mockImplementation(() => dispatchMock);

const navigationMock = jest.fn();
jest.spyOn(reactNavigation, 'useNavigation')
  .mockImplementation(() => ({ navigate: navigationMock}));

afterEach(() => {
  jest.clearAllMocks();
  NetInfo.fetch = () => Promise.resolve({ isConnected: true } as any);
});

describe('Testing ContentItem component', () => {

  it('should correctly render', async () => {
    const tree = renderer.create(<ContentItem contentItem={mockedContents[0]} />).toJSON();
    await flushPromises();

    expect(tree).toMatchSnapshot();
  });

  it('given contentItem has isFavorite = true, when onFavoriteToggle is called, removeFavoriteId is dispatched', async () => {
    const wrapper = shallow(<ContentItem contentItem={{ ...mockedContents[0], isFavorite: true }} />);
    wrapper.find('ContentHeader').props().onFavoriteToggle()

    await flushPromises();

    expect(dispatchMock).toHaveBeenCalledWith(removeFavoriteId(mockedContents[0].id));
    expect(dispatchMock).not.toHaveBeenCalledWith(setFavoriteId(mockedContents[0].id));
  });

  it('given contentItem has isFavorite = false, when onFavoriteToggle is called, setFavoriteId is dispatched', async () => {
    const wrapper = shallow(<ContentItem contentItem={{ ...mockedContents[0], isFavorite: false }} />);
    wrapper.find('ContentHeader').props().onFavoriteToggle()

    await flushPromises();

    expect(dispatchMock).not.toHaveBeenCalledWith(removeFavoriteId(mockedContents[0].id));
    expect(dispatchMock).toHaveBeenCalledWith(setFavoriteId(mockedContents[0].id));
  });

  it('given setIsHidden is called, setHiddenId is dispatched', async () => {
    const wrapper = shallow(<ContentItem contentItem={mockedContents[0]} />);
    wrapper.find('ContentHeader').props().setIsHidden();

    await flushPromises();

    expect(dispatchMock).toHaveBeenCalledWith(setHiddenId(mockedContents[0].id));
  });

  it('given onPress is called on ContentInfo, should navigate to contentDetails', async () => {
    const wrapper = shallow(<ContentItem contentItem={mockedContents[0]} />);
    wrapper.find('TouchableOpacity').props().onPress();

    await flushPromises();

    expect(navigationMock).toHaveBeenCalledWith(Routes.main.contentDetails, { contentItem: mockedContents[0] });
  });


});
