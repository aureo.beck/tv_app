

import React, { useCallback, useEffect, useState } from 'react';
import {
  FlatList, StyleSheet, View, Text, TextInput, SafeAreaView, ActivityIndicator,
} from 'react-native';
import { useSelector, useDispatch } from 'react-redux';
import Icon from 'react-native-vector-icons/MaterialIcons';
import NetInfo from '@react-native-community/netinfo';

import Colors from '../../constants/Colors';
import ContentItem from './ContentItem';
import Icons from '../../constants/Icons';
import Accessibility from '../../constants/Accessibility';
import Strings from '../../constants/Strings';
import { ContentItemType } from '../../state/ContentList/ContentList.types';
import { getContentsSearch, getFavoriteIds, getHiddenIds } from '../../state/ContentList/ContentList.actions';
import { selectNotHiddenContentWithFavorites } from '../../state/ContentList/ContentList.selectors';

const ContentList = () => {
  const [search, setSearch] = useState('');
  const [hasNetwork, setHasNetwork] = useState(true);

  const dispatch = useDispatch();
  const contentsSearch: Array<ContentItemType> = useSelector(selectNotHiddenContentWithFavorites);

  const fetchContents = useCallback(() => {
    dispatch(getContentsSearch(''));
    dispatch(getFavoriteIds());
    dispatch(getHiddenIds());
  }, []);

  useEffect(() => {
    NetInfo.fetch().then((state) => {
      if (state.isConnected) {
        fetchContents();
      } else {
        setHasNetwork(false);
      }
    });
    
    const netInfoListener = NetInfo.addEventListener((state) => {
      if (state.isConnected) {
        fetchContents();
      }
      setHasNetwork(state.isConnected);
    });
    return () => netInfoListener(); // unsubscribe
  }, []);

  useEffect(() => {
    if (search) {
      dispatch(getContentsSearch(search));
    }
  }, [search]);

  return (
    <SafeAreaView style={styles.container}>
      <View style={styles.content}>
        <View style={styles.searchInput}>
          <Icon name={Icons.search} size={30} color={Colors.white} backgroundColor={Colors.backgroundList} />
          <TextInput
            onChangeText={(newSearch) => setSearch(newSearch)}
            placeholder={Strings.searchPlaceholder}
            value={search}
            style={styles.textInput}
            placeholderTextColor={Colors.title}
            testID={Accessibility.searchInput}
          />
        </View>
        {hasNetwork ? (
          <FlatList
            data={contentsSearch}
            keyExtractor={(item) => item.id}
            renderItem={({ item }) => <ContentItem contentItem={item} />}
            ListEmptyComponent={(<ActivityIndicator size="large" color={Colors.primarySelection} />)}
          />
        ) : (
          <View style={styles.connectionError}>
            <Icon name={Icons.cloudOff} size={30} color={Colors.white} backgroundColor={Colors.backgroundList} />
            <Text style={styles.errorMessage} accessibilityLabel={Accessibility.textConnectionError}>{Strings.connectionError}</Text>
          </View>
        )}
      </View>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Colors.backgroundList,
  },
  content: {
    margin: 8,
  },
  searchInput: {
    flexDirection: 'row',
    alignItems: 'center',
    borderRadius: 20,
    borderColor: Colors.white,
    borderWidth: 1,
    marginBottom: 15,
    paddingLeft: 12,
    padding: 5,
  },
  textInput: {
    fontSize: 16,
    color: Colors.title,
    height: 40,
    marginLeft: 8,
  },
  errorMessage: {
    fontSize: 16,
    color: Colors.title,
    marginTop: 5,
  },
  connectionError: {
    alignItems: 'center',
    justifyContent: 'center',
  }
});

export default ContentList;
