

import React from 'react';
import {
  StyleSheet,
  View,
  TouchableOpacity,
} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialIcons';

import Colors from '../../constants/Colors';
import Icons from '../../constants/Icons';

interface Props {
  setIsHidden: () => void;
  isFavorite?: boolean,
  onFavoriteToggle: () => void;
}

const ContentHeader = ({ setIsHidden, isFavorite, onFavoriteToggle }: Props) => {
  return (
    <View style={styles.container}>
      <TouchableOpacity style={[styles.headerButton, styles.leftHeaderButton]} onPress={setIsHidden}>
        <Icon name={Icons.visibilityOff} size={30} color={Colors.white} />
      </TouchableOpacity>
      <TouchableOpacity style={[styles.headerButton, styles.rightHeaderButton]} onPress={onFavoriteToggle}>
        <Icon name={isFavorite ? Icons.favorite : Icons.favoriteBorder} size={30} color={Colors.primarySelection} />
      </TouchableOpacity>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    zIndex: 20,
  },
  headerButton: {
    zIndex: 99,
    position: 'absolute',
    margin: 5,
  },
  leftHeaderButton: {
    left: 0,
  },
  rightHeaderButton: {
    right: 0,
  }
});

export default ContentHeader;
