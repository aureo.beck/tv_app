

import React from 'react';
import {
  StyleSheet,
  View,
  Text,
} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialIcons';

import Colors from '../../constants/Colors';
import Icons from '../../constants/Icons';

interface Props {
  imDbRating?: string;
}

const ContentRating = ({ imDbRating }: Props) => {
  return (
    <View style={styles.ratingContainer}>
      <Icon name={Icons.star} size={18} color={Colors.rating} />
      <Text style={styles.rating}>{imDbRating}</Text>
    </View>
  );
};

const styles = StyleSheet.create({
  ratingContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    padding: 10,
    width: '15%',
  },
  rating: {
    fontSize: 16,
    color: Colors.title,
    marginLeft: 2,
  },
});

export default ContentRating;
