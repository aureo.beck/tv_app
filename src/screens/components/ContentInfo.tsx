

import React from 'react';
import {
  View,
  Text,
  StyleSheet,
} from 'react-native';

import ContentRating from './ContentRating';
import Colors from '../../constants/Colors';

interface Props {
  title: string;
  description: string;
  imDbRating?: string;
}

const ContentInfo = ({ title, description, imDbRating }: Props) => {
  return (
    <View style={styles.container}>
      <Text style={styles.title}>{title}{' '}{description}</Text>
      <ContentRating imDbRating={imDbRating} />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  title: {
    fontSize: 20,
    color: Colors.title,
  },
});

export default ContentInfo;
