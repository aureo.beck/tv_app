

import React, { useCallback } from 'react';
import {
  StyleSheet,
  View,
  Text,
  Image,
  SafeAreaView,
  ScrollView,
} from 'react-native';
import { useDispatch } from 'react-redux';

import ContentHeader from '../components/ContentHeader';
import ContentInfo from '../components/ContentInfo';
import { setFavoriteId, removeFavoriteId, setHiddenId } from '../../state/ContentList/ContentList.actions';
import Colors from '../../constants/Colors';

const ContentDetails = ({ route }) => {
  const {
    id,
    image,
    title,
    description,
    imDbRating,
    plot,
    isFavorite
  } = route.params.contentItem;

  const dispatch = useDispatch();

  const setIsHidden = useCallback(() => {
    dispatch(setHiddenId(id));
  }, []);

  const onFavoriteToggle = useCallback(() => {
    if (isFavorite) {
      dispatch(removeFavoriteId(id));
    } else {
      dispatch(setFavoriteId(id));
    }
  }, [isFavorite]);
  
  return (
    <SafeAreaView style={styles.container}>
      <ScrollView style={styles.scrollView}>
        <ContentHeader setIsHidden={setIsHidden} isFavorite={isFavorite} onFavoriteToggle={onFavoriteToggle} />
        <Image
          style={styles.imageStyle}
          source={{ uri: image }}
        />
        <View style={styles.contentInfo}>
          <ContentInfo title={title} description={description} imDbRating={imDbRating} />
          <Text style={styles.plot}>{plot}</Text>
        </View>
      </ScrollView>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 5,
    backgroundColor: Colors.backgroundList,
  },
  scrollView: {
    marginTop: 10,
  },
  imageStyle: {
    height: 480,
    flex: 1,
  },
  plot: {
    fontSize: 16,
    color: Colors.title,
    marginTop: 16,
  },
  contentInfo: {
    marginTop: 5,
    padding: 16,
  }
});

export default ContentDetails;
