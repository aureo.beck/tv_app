/* eslint-disable @typescript-eslint/no-explicit-any */
// TODO: Add redux types

import { createStore, applyMiddleware } from 'redux';
import createSagaMiddleware from 'redux-saga';
import { composeWithDevTools } from 'redux-devtools-extension/developmentOnly';
import applyAppStateListener from 'redux-enhancer-react-native-appstate';

import reducers from './reducers';
import rootSaga from './sagas';

const sagaMiddleware: any = createSagaMiddleware();
const composeEnhancers: any = composeWithDevTools({});

const store: any = createStore(
  reducers,
  composeEnhancers(applyAppStateListener(),
    applyMiddleware(sagaMiddleware)),
);

sagaMiddleware.run(rootSaga);

export default store;
