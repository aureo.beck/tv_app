

import { combineReducers } from 'redux';
import ContentList from './ContentList/ContentList.reducer';

const reducers = {
  ContentList,
};

export default combineReducers(reducers);
