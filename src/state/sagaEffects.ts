/* eslint-disable @typescript-eslint/no-explicit-any */
import * as Effects from "redux-saga/effects";

export const call: any = Effects.call;
export const fork: any = Effects.fork;
export const put: any = Effects.put;
export const takeLatest: any = Effects.takeLatest;
export const select: any = Effects.select;
export const debounce: any = Effects.debounce;
export const all: any = Effects.all;
