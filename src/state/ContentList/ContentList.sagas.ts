import { call, all, put, takeLatest, select, fork, debounce } from '../sagaEffects';

import { 
  GET_CONTENTS_SEARCH,
  GET_FAVORITE_IDS,
  SET_FAVORITE_ID,
  REMOVE_FAVORITE_ID,
  GET_HIDDEN_IDS,
  SET_HIDDEN_ID,
  REMOVE_HIDDEN_ID,
} from './ContentList.consts';
import { setContentsSearch, setFavoriteIds, setHiddenIds } from './ContentList.actions';
import { selectFavoriteIds, selectHiddenIds } from './ContentList.selectors';
import { fetchSearchResults } from '../../api/ImdbApiManager';
import { mockedContents } from './__mocks__/ContentList.mocks';
import AsyncStorageKeys from '../../constants/AsyncStorageKeys';
import { getItemAsyncStorage, setItemAsyncStorage } from '../../utils/AsyncStorageUtils';

import { ContentItemType } from './ContentList.types';

export function* fetchContentsSearch(action) {
  const searchExpression: string = action.payload;
  try {
    const contents: Array<ContentItemType> = yield call(fetchSearchResults, searchExpression);
    yield put(setContentsSearch(contents));
  } catch (error) {
    // In case API limit is reached (100 a day), showing mock data
    yield put(setContentsSearch(mockedContents));
  }
}

export function* fetchFavoriteIds() {
  try {
    const favoriteIds = yield call(getItemAsyncStorage, AsyncStorageKeys.FAVORITES_IDS);
    yield put(setFavoriteIds(favoriteIds));
  } catch (error) {
    console.error(error);
  }
}

export function* addContentIdToFavoriteIds(action) {
  const id: string = action.payload;

  const currentFavoriteIds: Array<string> = yield select(selectFavoriteIds);
  const newFavoriteIds = [...currentFavoriteIds, id];
  try {
    yield fork(setItemAsyncStorage, AsyncStorageKeys.FAVORITES_IDS, newFavoriteIds);
    yield put(setFavoriteIds(newFavoriteIds));
  } catch (error) {
    console.error(error);
  }
}

export function* removeContentIdFromFavoriteIds(action) {
  const id: string = action.payload;

  const favoriteIds: Array<string> = yield select(selectFavoriteIds);
  const currentFavoriteIds = [...favoriteIds];

  const indexToRemove = currentFavoriteIds.findIndex((favoriteId: string) => favoriteId === id)
  currentFavoriteIds.splice(indexToRemove, 1);

  try {
    yield fork(setItemAsyncStorage, AsyncStorageKeys.FAVORITES_IDS, currentFavoriteIds);
    yield put(setFavoriteIds(currentFavoriteIds));
  } catch (error) {
    console.error(error);
  }
}

export function* fetchHiddenIds() {
  try {
    const hiddenIds = yield call(getItemAsyncStorage, AsyncStorageKeys.HIDDEN_IDS);
    yield put(setHiddenIds(hiddenIds));
  } catch (error) {
    console.error(error);
  }
}

export function* addContentIdToHiddenIds(action) {
  const id: string = action.payload;

  const currentHiddenIds: Array<string> = yield select(selectHiddenIds);
  const newHiddenIds = [...currentHiddenIds, id];
  try {
    yield fork(setItemAsyncStorage, AsyncStorageKeys.HIDDEN_IDS, newHiddenIds);
    yield put(setHiddenIds(newHiddenIds));
  } catch (error) {
    console.error(error);
  }
}

export function* removeContentIdFromHiddenIds(action) {
  const id: string = action.payload;

  const hiddenIds: Array<string> = yield select(selectHiddenIds);
  const currentHiddenIds = [...hiddenIds];

  const indexToRemove = currentHiddenIds.findIndex((hiddenId: string) => hiddenId === id)
  currentHiddenIds.splice(indexToRemove, 1);

  try {
    yield fork(setItemAsyncStorage, AsyncStorageKeys.HIDDEN_IDS, currentHiddenIds);
    yield put(setHiddenIds(currentHiddenIds));
  } catch (error) {
    console.error(error);
  }
}

function* watchContentsSearchFetching() {
  yield debounce(1000, GET_CONTENTS_SEARCH, fetchContentsSearch);
}

function* watchFavoriteFetching() {
  yield takeLatest(GET_FAVORITE_IDS, fetchFavoriteIds);
}

function* watchSetFavoriteId() {
  yield takeLatest(SET_FAVORITE_ID, addContentIdToFavoriteIds);
}

function* watchRemoveFavoriteId() {
  yield takeLatest(REMOVE_FAVORITE_ID, removeContentIdFromFavoriteIds);
}

function* watchHiddenFetching() {
  yield takeLatest(GET_HIDDEN_IDS, fetchHiddenIds);
}

function* watchSetHiddenId() {
  yield takeLatest(SET_HIDDEN_ID, addContentIdToHiddenIds);
}

function* watchRemoveHiddenId() {
  yield takeLatest(REMOVE_HIDDEN_ID, removeContentIdFromHiddenIds);
}

export default function* watchContentList() {
  yield all([
    watchContentsSearchFetching(),
    watchFavoriteFetching(),
    watchHiddenFetching(),
    watchSetFavoriteId(),
    watchRemoveFavoriteId(),
    watchHiddenFetching(),
    watchSetHiddenId(),
    watchRemoveHiddenId(),
  ]);
}
