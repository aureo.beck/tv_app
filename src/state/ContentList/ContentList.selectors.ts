import { createSelector } from 'reselect';

import { getNotHiddenWithFavoriteContents } from './ContentList.helpers';

const selectContentsSearch = (state) => state.ContentList.contentsSearch;
const selectFavoriteIds = (state) => state.ContentList.favoriteIds;
const selectHiddenIds = (state) => state.ContentList.hiddenIds;

const selectNotHiddenContentWithFavorites = createSelector(
  [
    selectContentsSearch,
    selectFavoriteIds,
    selectHiddenIds,
  ],
  (contentsSearch,
    favoriteIds,
    hiddenIds) => getNotHiddenWithFavoriteContents(
    contentsSearch,
    favoriteIds,
    hiddenIds,
  ),
);

export {
  selectContentsSearch,
  selectFavoriteIds,
  selectHiddenIds,
  selectNotHiddenContentWithFavorites,
};
