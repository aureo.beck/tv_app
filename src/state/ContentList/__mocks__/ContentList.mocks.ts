
import { ContentItemType } from '../ContentList.types';

export const newId = ['newId123'];
export const mockedHiddenIds = ['tt11198330'];
export const mockedFavoriteIds = ['tt1751634'];

export const mockedContents: Array<ContentItemType> = [
  {
      "id": "tt11198330",
      "image": "https://m.media-amazon.com/images/M/MV5BZDBkZjRiNGMtZGU2My00ODdkLWI0MGYtNGU4MmJjN2MzOTkxXkEyXkFqcGdeQXVyMDM2NDM2MQ@@._V1_Ratio0.6837_AL_.jpg",
      "title": "House of the Dragon",
      "description": "(2022– )",
      "imDbRating": "8.9",
      "plot": "House of the Dragon tells the story of an internal succession war within House Targaryen at the height of its power, 172 years before the birth of Daenerys Targaryen."
  },
  {
      "id": "tt1751634",
      "image": "https://m.media-amazon.com/images/M/MV5BYTM0NjZjYjItM2JiYS00NmU5LWJmMTMtZjQ0OWU3Mzk1ZWZjXkEyXkFqcGdeQXVyMTAxNDE3MTE5._V1_Ratio0.6837_AL_.jpg",
      "title": "The Sandman",
      "description": "(2022– )",
      "imDbRating": "7.8",
      "plot": "Upon escaping after decades of imprisonment by a mortal wizard, Dream, the personification of dreams, sets about to reclaim his lost equipment."
  },
  {
      "id": "tt10857160",
      "image": "https://m.media-amazon.com/images/M/MV5BMjU4MTkxNzktNzUyYy00NDM2LWE5NGQtNjJlN2Q0N2MxZDAxXkEyXkFqcGdeQXVyMTkxNjUyNQ@@._V1_Ratio0.6837_AL_.jpg",
      "title": "She-Hulk: Attorney at Law",
      "description": "(2022– )",
      "imDbRating": "5.1",
      "plot": "Jennifer Walters navigates the complicated life of a single, 30-something attorney who also happens to be a green 6-foot-7-inch superpowered Hulk."
  },
  {
      "id": "tt3032476",
      "image": "https://m.media-amazon.com/images/M/MV5BMTMxOGM0NzItM2E0OS00YWYzLWEzNzUtODUzZTBjM2I4MTZkXkEyXkFqcGdeQXVyMTM1MTE1NDMx._V1_Ratio0.6837_AL_.jpg",
      "title": "Better Call Saul",
      "description": "(2015–2022)",
      "imDbRating": "8.9",
      "plot": "The trials and tribulations of criminal lawyer Jimmy McGill before his fateful run-in with Walter White and Jesse Pinkman."
  },
  {
      "id": "tt0944947",
      "image": "https://m.media-amazon.com/images/M/MV5BYTRiNDQwYzAtMzVlZS00NTI5LWJjYjUtMzkwNTUzMWMxZTllXkEyXkFqcGdeQXVyNDIzMzcwNjc@._V1_Ratio0.7245_AL_.jpg",
      "title": "Game of Thrones",
      "description": "(2011–2019)",
      "imDbRating": "9.2",
      "plot": "Nine noble families fight for control over the lands of Westeros, while an ancient enemy returns after being dormant for millennia."
  },
  {
      "id": "tt7631058",
      "image": "https://m.media-amazon.com/images/M/MV5BMmVlODAyNTAtODc3Yi00MjFhLTk5MTktNWIwOTUzY2M2ZDc5XkEyXkFqcGdeQXVyMDM2NDM2MQ@@._V1_Ratio0.6837_AL_.jpg",
      "title": "The Lord of the Rings: The Rings of Power",
      "description": "(2022– )",
      "imDbRating": undefined,
      "plot": undefined
  },
  {
      "id": "tt1745960",
      "image": "https://m.media-amazon.com/images/M/MV5BOWQwOTA1ZDQtNzk3Yi00ZmVmLWFiZGYtNjdjNThiYjJhNzRjXkEyXkFqcGdeQXVyODE5NzE3OTE@._V1_Ratio0.6837_AL_.jpg",
      "title": "Top Gun: Maverick",
      "description": "(2022)",
      "imDbRating": "8.5",
      "plot": "After more than thirty years of service as one of the Navy's top aviators, Pete Mitchell is where he belongs, pushing the envelope as a courageous test pilot and dodging the advancement in rank that would ground him."
  },
  {
      "id": "tt10954984",
      "image": "https://m.media-amazon.com/images/M/MV5BNGM1MDc3ZjgtODlkOS00NmZjLWJlOTItNGQ5OGFhN2JlNjgxXkEyXkFqcGdeQXVyNjk1Njg5NTA@._V1_Ratio0.7857_AL_.jpg",
      "title": "Nope",
      "description": "(2022)",
      "imDbRating": "7.1",
      "plot": "The residents of a lonely gulch in inland California bear witness to an uncanny and chilling discovery."
  },
  {
      "id": "tt14954498",
      "image": "https://m.media-amazon.com/images/M/MV5BNjU1OTg2MDgtMjE5Yi00YzVkLTg1NGYtYWVmMjY0MTRiOTQ5XkEyXkFqcGdeQXVyMTEzMTI1Mjk3._V1_Ratio0.7857_AL_.jpg",
      "title": "Echoes",
      "description": "(2022)",
      "imDbRating": "5.8",
      "plot": "Leni and Gina are identical twins who have secretly swapped their lives since they were children, culminating in a double life as adults, but one of the sisters goes missing and everything in their perfectly schemed world turns into chaos."
  },
  {
      "id": "tt12851524",
      "image": "https://m.media-amazon.com/images/M/MV5BZTFmZWUwZmEtYzc4Ni00N2FmLTk1MmMtZmMyODYwZTNkY2EwXkEyXkFqcGdeQXVyMTM1MTE1NDMx._V1_Ratio0.7857_AL_.jpg",
      "title": "Only Murders in the Building",
      "description": "(2021– )",
      "imDbRating": "8.1",
      "plot": "Three strangers who share an obsession with true crime suddenly find themselves caught up in one."
  }
];

  