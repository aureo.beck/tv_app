/* eslint-disable @typescript-eslint/no-explicit-any */
// TODO: Add redux types

import {
  SET_CONTENTS_SEARCH,
  SET_FAVORITE_IDS,
  SET_HIDDEN_IDS,
} from './ContentList.consts';

import {
  ContentItemType,
} from './ContentList.types';

interface StateType {
  contentsSearch: Array<ContentItemType>;
  favoriteIds: Array<string>;
  hiddenIds: Array<string>;
}

const initialState: StateType = {
  contentsSearch: [],
  favoriteIds: [],
  hiddenIds: [],
};

const contentListReducer = (
  state: StateType = initialState,
  action: any,
): StateType => {
  switch (action.type) {
    case SET_CONTENTS_SEARCH: {
      return {
        ...state,
        contentsSearch: action.payload,
      };
    }
    case SET_FAVORITE_IDS: {
      return {
        ...state,
        favoriteIds: action.payload,
      };
    }
    case SET_HIDDEN_IDS: {
      return {
        ...state,
        hiddenIds: action.payload,
      };
    }
    default:
      return state;
  }
};

export default contentListReducer;
