

import {
  GET_CONTENTS_SEARCH,
  SET_CONTENTS_SEARCH,
  GET_FAVORITE_IDS,
  SET_FAVORITE_IDS,
  SET_FAVORITE_ID,
  REMOVE_FAVORITE_ID,
  GET_HIDDEN_IDS,
  SET_HIDDEN_IDS,
  SET_HIDDEN_ID,
  REMOVE_HIDDEN_ID,
} from './ContentList.consts';

import {
  ContentItemType,
} from './ContentList.types';

const getContentsSearch = (payload: string) => ({
  type: GET_CONTENTS_SEARCH,
  payload,
});

const setContentsSearch = (payload: Array<ContentItemType>) => ({
  type: SET_CONTENTS_SEARCH,
  payload,
});

const getFavoriteIds = () => ({
  type: GET_FAVORITE_IDS,
});

const setFavoriteIds = (payload: Array<string>) => {
  return ({
  type: SET_FAVORITE_IDS,
  payload,
})};

const setFavoriteId = (payload: string) => ({
  type: SET_FAVORITE_ID,
  payload,
});

const removeFavoriteId = (payload: string) => ({
  type: REMOVE_FAVORITE_ID,
  payload,
});

const getHiddenIds = () => ({
  type: GET_HIDDEN_IDS,
});

const setHiddenIds = (payload: Array<string>) => ({
  type: SET_HIDDEN_IDS,
  payload,
});

const setHiddenId = (payload: string) => ({
  type: SET_HIDDEN_ID,
  payload,
});

const removeHiddenId = (payload: string) => ({
  type: REMOVE_HIDDEN_ID,
  payload,
});

export {
  getContentsSearch,
  setContentsSearch,
  getFavoriteIds,
  setFavoriteIds,
  setFavoriteId,
  removeFavoriteId,
  getHiddenIds,
  setHiddenIds,
  setHiddenId,
  removeHiddenId,
};
