

export const GET_CONTENTS_SEARCH = '@contentList/GET_CONTENTS_SEARCH';
export const SET_CONTENTS_SEARCH = '@contentList/SET_CONTENTS_SEARCH';
export const GET_FAVORITE_IDS = '@contentList/GET_FAVORITE_IDS';
export const SET_FAVORITE_ID = '@contentList/SET_FAVORITE_ID';
export const SET_FAVORITE_IDS = '@contentList/SET_FAVORITE_IDS';
export const REMOVE_FAVORITE_ID = '@contentList/REMOVE_FAVORITE_ID';
export const GET_HIDDEN_IDS = '@contentList/GET_HIDDEN_IDS';
export const SET_HIDDEN_ID = '@contentList/SET_HIDDEN_ID';
export const SET_HIDDEN_IDS = '@contentList/SET_HIDDEN_IDS';
export const REMOVE_HIDDEN_ID = '@contentList/REMOVE_HIDDEN_ID';
