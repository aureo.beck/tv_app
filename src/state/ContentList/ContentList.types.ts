

export interface ContentItemType {
  id: string;
  image: string;
  title: string;
  description: string;
  imDbRating?: string;
  plot?: string;
  isFavorite?: boolean;
}
