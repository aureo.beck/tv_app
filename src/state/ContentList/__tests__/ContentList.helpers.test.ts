
import {
  getNotHiddenContents,
  addFavoriteToContents,
  getNotHiddenWithFavoriteContents,
} from '../ContentList.helpers';

import {
  mockedContents,
  mockedFavoriteIds,
  mockedHiddenIds,
} from '../__mocks__/ContentList.mocks';

describe('Testing ContentList.helpers', () => {
  afterEach(() => {
    jest.clearAllMocks();
  });

  it('Test getNotHiddenContents', () => {
    const result = getNotHiddenContents(mockedContents, mockedHiddenIds);
    expect(result).toEqual(mockedContents.slice(1));
  });

  it('Test addFavoriteToContents', () => {
    const output = [...mockedContents];
    output[1] = { ...output[1], isFavorite: true };
    const result = addFavoriteToContents(mockedContents, mockedFavoriteIds);
    expect(result).toEqual(output);
  });

  it('Test getNotHiddenWithFavoriteContents', () => {
    const output = [...mockedContents];
    const notHiddenOutput = output.slice(1);
    notHiddenOutput[0] = { ...notHiddenOutput[0], isFavorite: true };
    const result = getNotHiddenWithFavoriteContents(mockedContents, mockedFavoriteIds, mockedHiddenIds);
    expect(result).toEqual(notHiddenOutput);
  });
});
