
import { select } from 'redux-saga/effects';
import { expectSaga } from 'redux-saga-test-plan';
import * as matchers from 'redux-saga-test-plan/matchers';
import { fetchSearchResults } from '../../../api/ImdbApiManager';
import { getItemAsyncStorage, setItemAsyncStorage } from '../../../utils/AsyncStorageUtils';

import {
  SET_CONTENTS_SEARCH,
  SET_FAVORITE_IDS,
  SET_HIDDEN_IDS,
} from '../ContentList.consts';
import { selectFavoriteIds, selectHiddenIds } from '../ContentList.selectors';
import {
  fetchContentsSearch,
  fetchFavoriteIds,
  addContentIdToFavoriteIds,
  removeContentIdFromFavoriteIds,
  fetchHiddenIds,
  addContentIdToHiddenIds,
  removeContentIdFromHiddenIds,
} from '../ContentList.sagas';
import {
  mockedContents,
  mockedFavoriteIds,
  mockedHiddenIds,
  newId,
} from '../__mocks__/ContentList.mocks';

import AsyncStorageKeys from '../../../constants/AsyncStorageKeys';

describe('Testing ContentList.sagas', () => {
  afterEach(() => {
    jest.clearAllMocks();
  });

  it('Test fetchContentsSearch', () => {
    const searchExpression = 'searchExpression123';
    return expectSaga(fetchContentsSearch, { payload: searchExpression })
      .provide([
        [matchers.call.fn(fetchSearchResults), mockedContents],
      ])
      .put({
        type: SET_CONTENTS_SEARCH,
        payload: mockedContents,
      })
      .run();
  });

  it('Test fetchFavoriteIds', () => {
    return expectSaga(fetchFavoriteIds)
      .provide([
        [matchers.call.fn(getItemAsyncStorage), mockedFavoriteIds],
      ])
      .put({
        type: SET_FAVORITE_IDS,
        payload: mockedFavoriteIds,
      })
      .run();
  });

  it('Test addContentIdToFavoriteIds', () => {
    return expectSaga(addContentIdToFavoriteIds, { payload: newId })
      .provide([
        [select(selectFavoriteIds), mockedFavoriteIds],
        [matchers.fork.fn(setItemAsyncStorage), undefined],
      ])
      .fork(setItemAsyncStorage, AsyncStorageKeys.FAVORITES_IDS, [...mockedFavoriteIds, newId])
      .put({
        type: SET_FAVORITE_IDS,
        payload: [...mockedFavoriteIds, newId],
      })
      .run();
  });

  it('Test removeContentIdFromFavoriteIds', () => {
    const idToBeRemoved = mockedFavoriteIds[0];
    return expectSaga(removeContentIdFromFavoriteIds, { payload: idToBeRemoved })
      .provide([
        [select(selectFavoriteIds), mockedFavoriteIds],
        [matchers.fork.fn(setItemAsyncStorage), undefined],
      ])
      .fork(setItemAsyncStorage, AsyncStorageKeys.FAVORITES_IDS, mockedFavoriteIds.slice(1))
      .put({
        type: SET_FAVORITE_IDS,
        payload: mockedFavoriteIds.slice(1),
      })
      .run();
  });

  it('Test fetchHiddenIds', () => {
    return expectSaga(fetchHiddenIds)
      .provide([
        [matchers.call.fn(getItemAsyncStorage), mockedHiddenIds],
      ])
      .put({
        type: SET_HIDDEN_IDS,
        payload: mockedHiddenIds,
      })
      .run();
  });

  it('Test addContentIdToHiddenIds', () => {
    return expectSaga(addContentIdToHiddenIds, { payload: newId })
      .provide([
        [select(selectHiddenIds), mockedHiddenIds],
        [matchers.fork.fn(setItemAsyncStorage), undefined],
      ])
      .fork(setItemAsyncStorage, AsyncStorageKeys.HIDDEN_IDS, [...mockedHiddenIds, newId])
      .put({
        type: SET_HIDDEN_IDS,
        payload: [...mockedHiddenIds, newId],
      })
      .run();
  });

  it('Test removeContentIdFromHiddenIds', () => {
    const idToBeRemoved = mockedHiddenIds[0];
    return expectSaga(removeContentIdFromHiddenIds, { payload: idToBeRemoved })
      .provide([
        [select(selectHiddenIds), mockedHiddenIds],
        [matchers.fork.fn(setItemAsyncStorage), undefined],
      ])
      .fork(setItemAsyncStorage, AsyncStorageKeys.HIDDEN_IDS, mockedHiddenIds.slice(1))
      .put({
        type: SET_HIDDEN_IDS,
        payload: mockedHiddenIds.slice(1),
      })
      .run();
  });
});
