import { sortByBooleanProperty } from '../../utils/sortByProperty';
import { ContentItemType } from './ContentList.types';

export const getNotHiddenContents = (
  contents: Array<ContentItemType>,
  hiddenIds: Array<string>,
): Array<ContentItemType> => {
  return contents.filter(content => !hiddenIds.some((hiddenId: string) => content.id === hiddenId))
};

export const addFavoriteToContents = (
  contents: Array<ContentItemType>,
  favoriteIds: Array<string>,
): Array<ContentItemType> => {
  const currentContents = [...contents];

  favoriteIds.forEach((favoriteId: string) => {
    const indexToAdd = currentContents.findIndex((currentContent: ContentItemType) => currentContent.id === favoriteId);
    currentContents[indexToAdd] = {
      ...currentContents[indexToAdd],
      isFavorite: true,
    }
  });

  return currentContents;
};

export const getNotHiddenWithFavoriteContents = (
  contents: Array<ContentItemType>,
  favoriteIds: Array<string>,
  hiddenIds: Array<string>,
  ): Array<ContentItemType> => {
  const withFavoriteContents = addFavoriteToContents(contents, favoriteIds);
  const notHiddenContents = getNotHiddenContents(withFavoriteContents, hiddenIds);
  const sortedContentsByRatings = sortByBooleanProperty(notHiddenContents, 'isFavorite');

  return sortedContentsByRatings;
};
