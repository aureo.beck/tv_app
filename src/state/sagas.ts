
import { all } from 'redux-saga/effects';

import ContentListSaga from './ContentList/ContentList.sagas';

export default function* rootSaga() {
  yield all([
    ContentListSaga(),
  ]);
}
