
import React, { FC } from 'react';
import { Provider } from 'react-redux';
import { NavigationContainer } from '@react-navigation/native';
import { SafeAreaProvider } from 'react-native-safe-area-context';

import store from './src/state';
import RootSwitch from './src/navigator/Navigators';

const App: FC = () => (
  <Provider store={store}>
    <SafeAreaProvider>
      <NavigationContainer>
        <RootSwitch />
      </NavigationContainer>
    </SafeAreaProvider>
  </Provider>
);

export default App;
