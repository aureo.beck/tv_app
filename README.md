# TvApp

## Requirements

- [Node](https://nodejs.org/en/download/) (14.20.0)
- [Xcode](https://developer.apple.com/xcode/) (13.1)
- [Cocoa Pods](https://guides.cocoapods.org/using/getting-started.html) (1.11.3)
- [Java](https://www.oracle.com/java/technologies/downloads/) (11)
- [Android Studio](https://developer.android.com/studio) (*latest*)
- [Visual Studio Code](https://code.visualstudio.com/download) (*latest*)
- [Git](https://git-scm.com/downloads) (*latest*)

## Installation

Please follow react native [doc](https://reactnative.dev/docs/environment-setup) to get started.

To fetch dependencies run:

``` txt
npm install or yarn install
```

``` txt
cd ios
pod install
```

## Running

To start your application you can use Android Studio / Xcode or run:

``` txt
npx react-native run-ios
npx react-native run-android
```

## API

Documentation for IMDB api can be found [here](https://imdb-api.com/api#AdvancedSearch-header)

There's a limit of 100 requests per day. In case this limit is reached, app will show mock data.

## Testing

All functions should be tested, covering all scenarios of it's use:

``` txt
npm test
```

For coverage info run:
``` txt
npx jest --coverage
```

To run a specific test:
``` txt
npm test  -- -t 'Global or individual test name'
```

## Linter

In order to run linter you need use `npm run lint`.

If you want to fix problems as well run `npm run lint-fix`

We recommend an installion of VS Code [eslint plugin](https://marketplace.visualstudio.com/items?itemName=dbaeumer.vscode-eslint) if you use this text editor

## Style guide

[Airbnb style](https://github.com/airbnb/javascript)
[Naming guide](https://betterprogramming.pub/a-useful-framework-for-naming-your-classes-functions-and-variables-e7d186e3189f)
