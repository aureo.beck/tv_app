
console.log = jest.fn();
console.error = jest.fn();

import '@testing-library/jest-dom';
import '@testing-library/react-native';

jest.mock('@react-navigation/native', () => {
  const actualModule = jest.requireActual('@react-navigation/native');

  return {
    __esModule: true,
    ...actualModule,
    useNavigation: () => ({
      navigate: jest.fn(),
    }),
  };
});

jest.mock('@react-native-community/netinfo', () => ({
  fetch: () => Promise.resolve({ isConnected: true }),
  addEventListener: jest.fn(),
}));

jest.mock('rn-fetch-blob', () => ({
  fetch: () => Promise.resolve({}),
}));

jest.mock('react-native-vector-icons/MaterialIcons', () => jest.fn());

jest.mock('react-native', () => ({
  NativeModules: {
    SettingsManager: {
      settings: {
        AppleLanguages: ['en-US', 'en'],
        AppleLocale: 'en_US',
      },
    },
    I18nManager: {
      localeIdentifier: 'en_US',
    },
  },
  Platform: {
    os: 'ios',
  },
  Alert: {
    alert: jest.fn(),
  },
  AppState: {
    alert: jest.fn(),
  },
}));

jest.mock('@react-native-async-storage/async-storage', () => ({
  setItem: jest.fn(() => Promise.resolve(true)),
  getItem: jest.fn(() => Promise.resolve('123')),
  mergeItem: jest.fn(() => Promise.resolve(true)),
  removeItem: jest.fn(() => Promise.resolve(true)),
}));

export const mockDateStr = '2016-06-20T16:08:10Z';
export const mockDateNum = 20160620;

export const mockDateStrOneMonthAhead = '2016-07-20T16:08:10Z';
export const mockDateStrOneMonthBack = '2016-05-20T16:08:10Z';

const mockDate = new Date(mockDateStr);
Date.now = jest.fn(() => new Date(mockDateStr));

jest.mock('redux-enhancer-react-native-appstate', () => ({
  FOREGROUND: 'FOREGROUND',
}));
